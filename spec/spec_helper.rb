require 'rspec'
require 'net/http'
require 'rest-client'
require_relative '../lib/falcor'
require 'json'

RSpec.configure do |config|
  config.full_backtrace = true
end
