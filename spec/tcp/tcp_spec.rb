RSpec.describe Falcor::Tcp do
  around do |example|
    begin
      @falcor = Falcor::Tcp.new
      example.run
    ensure
      @falcor.reset!
    end
  end

  let(:default_response) do
    doc = <<~HERE
      HTTP/1.1 200 OK
      Content-Length: 1
  
      H
    HERE
    doc.split("\n").map(&:strip).join("\n")
  end

  context 'RestClient' do
    it 'should intercept' do
      @falcor.intercept 'www.example.com', 80 do |interceptor|
        # server response
        interceptor.on(:read_nonblock) do |*_|
          default_response
        end

        # client sending payload
        interceptor.on(:write) do |message, *_|
          expect(JSON.parse(message.raw)).to eql({ 'hello' => 'world'})
        end
      end
      data = RestClient.post "http://www.example.com", { hello: :world }.to_json
      expect(data.body).to eql("H")
    end
  end

  context 'Net::HTTP' do
    it 'should intercept' do
      @falcor.intercept 'www.example.com', 80 do |interceptor|
        # server response
        interceptor.on(:read_nonblock) do |*_|
          default_response
        end

        interceptor.on(:write_nonblock) do |message, *_|
          if message.http_headers?
            expect(message.http_method).to eql("POST")
          else
            expect(JSON.parse(message.raw)).to eql({ 'hello' => 'world'})
          end
        end
      end
      data = Net::HTTP.post URI.parse("http://www.example.com"), { hello: :world }.to_json
      expect(data.body).to eql("H")
    end

    it 'should filter request body matches' do
      @falcor.intercept 'www.example.com', 80, match: /hello/ do |interceptor|
        interceptor.on(:write_nonblock) do |message, *_|
          expect(JSON.parse(message.raw)).to eql({ 'hello' => 'world'})
        end
      end
      Net::HTTP.post URI.parse("http://www.example.com"), { hello: :world }.to_json
    end

    it 'should throw error if url is different' do
      @falcor.intercept 'www.example2.com', 80, match: /hello/ do |_|
      end
      expect { Net::HTTP.post URI.parse("http://www.example.com"), { hello: :world }.to_json }.to raise_error do |error|
        expect(error.class).to be(Falcor::TcpError)
      end
    end
  end
end