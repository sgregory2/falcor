require_relative './falcor/tcp'

module Falcor
  def self.configure(&block)
    block.call(config)
  end

  def self.config
    Config.config
  end

  class Config
    attr_accessor :raises_on_missing, :default_response_headers, :default_response_body

    def self.config
      @config ||= new
    end

    def initialize
      @raises_on_missing = false
      @default_response_headers = nil
      @default_response_body = nil
    end
  end
end