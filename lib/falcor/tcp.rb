require_relative './interceptors'
require_relative './message'

module Falcor
  class TcpError < StandardError; end
  class Tcp

    DEFAULT_RESPONSE = <<~DATA
      HTTP/1.1 200 OK
      Content-Length: 1
      
      H
    DATA

    attr_reader :interceptors

    def self.config
      Config.config
    end

    def initialize
      @interceptors = Interceptors.new
      original_tcp_socket = TCPSocket.method(:new)
      mock_tcp_socket!
      @reset_proc = lambda do
        TCPSocket.define_singleton_method(:open) do |*args|
          original_tcp_socket.call(*args)
        end
      end
    end

    def intercept(url, port, method: 'ALL', match: nil, &block)
      @interceptors.add(url, port, method: method, matches: match, &block)
    end

    def reset!
      @reset_proc.call
      @interceptors = nil
    end

    def mock_tcp_socket!
      this = self
      TCPSocket.define_singleton_method(:open) do |url, port, *args|
        raise TcpError, "No Interceptors declared for #{url}:#{port}" unless this.interceptors.locate(url, port)

        klass = Class.new do
          attr_reader :url, :port, :intercept

          define_method :initialize do |url, port, local_url, local_port|
            @url = url
            @port = port
            @intercept = this
          end

          def setsockopt(*_); end

          def closed?
            false
          end

          def close
            true
          end

          def to_io; end

          define_method :write do |payload|
            intercept.interceptors.locate(url, port, body: payload)&.write&.call(Message.new(payload))
            payload.size
          end

          define_method :write_nonblock do |data, *args|
            intercept.interceptors.locate(url, port, body: data)&.write_nonblock&.call(Message.new(data), *args)
            data.size
          end

          define_method :read_nonblock do |buffer_size, *args|
            intercept.interceptors.locate(url, port)&.read_nonblock&.call(buffer_size, *args) || DEFAULT_RESPONSE
          end
        end
        klass.new(url, port, *args)
      end
    end
  end
end