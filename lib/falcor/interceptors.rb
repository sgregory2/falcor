module Falcor
  class Interceptors
    include Enumerable

    def initialize
      @interceptors = []
    end

    def add(
      url,
      port,
      method: 'ALL',
      matches: nil,
      &block
    )
      interceptor = Interceptor.new(url, port, method, matches)
      block.call(interceptor)
      @interceptors << interceptor
    end

    def locate(url, port, body: nil)
      @interceptors.find do |interceptor|
        if interceptor.url === url && interceptor.port === port
          body.nil? ? true : !!(interceptor.matches =~ body)
        end
      end
    end

    def each
      @interceptors.each do |interceptor|
        yield interceptor
      end
    end
  end

  class Interceptor
    attr_reader :url, :method, :matches, :port

    def initialize(url, port, method, body_matches = nil)
      @url = url
      @port = port
      @method = method
      @matches = body_matches
    end

    def on(method, &block)
      send("on_#{method}", &block)
    end

    %i[read_nonblock write_nonblock write read].each do |method|
      define_method method do
        instance_variable_get("@#{method}")
      end

      define_method "#{method}=" do |proc|
        instance_variable_set("@#{method}", proc)
      end

      define_method "on_#{method}" do |&block|
        send("#{method}=", ->(*args) { block.call(*args) })
      end
    end
  end
end