module Falcor
  class Message
    attr_reader :raw
    def initialize(body)
      @raw = body
    end

    def http_headers?
      @raw.split("\n")[0].include? 'HTTP'
    end

    def http_method
      @raw.scan(/^(\w+)\s/)&.[](0)&.[](0)
    end
  end
end